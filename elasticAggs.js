const rp = require('request-promise');

const SQL_QUERY = `
    select
        symbol,tradeDate,
        sum(case when side = 1 then quantity else 0 end) as buyBase,
        sum(case when side = -1 then quantity else 0 end) as sellBase,
        sum(case when side = 1 then quantity * price else 0 end) as sellQuote,
        sum(case when side = -1 then quantity * price else 0 end) as buyQuote
    from
        Trades
    where 
        apiKeyId in (5,6,7)
    group by 
        symbol, tradeDate;
    `

const ELASTIC_COOL_QUERY = {        //this is Elastic Search analog of previous SQL query
  "query" : {
            "bool" : {
                "filter" : {
                    "terms" : {
                        "apiKeyId" : [5,6,7]
                    }
                }
            }
        },
        "aggs":{
            "bySymbol": {
                "terms": {
                    "field": "symbol",
                    "size": 100
                },
                "aggs": {
                    "byTradeDate": {
                        "terms": {
                            "field": "tradeDate",
                            "size": 1000
                        },
                      "aggs": {
                        "buyBase": {
                            "scripted_metric" : {
                                "init_script" : "state.buyBase = []",
                                "map_script" : "state.buyBase.add(doc.side.value == 1 ? doc.quantity.value : 0)",
                                "combine_script" : "double buyBase = 0; for (t in state.buyBase) { buyBase += t } return buyBase",
                                "reduce_script" : "double buyBase = 0; for (a in states) { buyBase += a } return buyBase"
                            }
                        },
                        "sellBase": {
                            "scripted_metric" : {
                                "init_script" : "state.sellBase = []",
                                "map_script" : "state.sellBase.add(doc.side.value == -1 ? doc.quantity.value : 0)",
                                "combine_script" : "double sellBase = 0; for (t in state.sellBase) { sellBase += t } return sellBase",
                                "reduce_script" : "double sellBase = 0; for (a in states) { sellBase += a } return sellBase"
                            }
                        },
                        "buyQuote": {
                            "scripted_metric" : {
                                "init_script" : "state.buyQuote = []",
                                "map_script" : "state.buyQuote.add(doc.side.value == 1 ? doc.quantity.value * doc.price.value : 0)",
                                "combine_script" : "double buyQuote = 0; for (t in state.buyQuote) { buyQuote += t } return buyQuote",
                                "reduce_script" : "double buyQuote = 0; for (a in states) { buyQuote += a } return buyQuote"
                            }
                        },
                        "sellQuote": {
                            "scripted_metric" : {
                                "init_script" : "state.sellQuote = []",
                                "map_script" : "state.sellQuote.add(doc.side.value == -1 ? doc.quantity.value * doc.price.value : 0)",
                                "combine_script" : "double sellQuote = 0; for (t in state.sellQuote) { sellQuote += t } return sellQuote",
                                "reduce_script" : "double sellQuote = 0; for (a in states) { sellQuote += a } return sellQuote"
                            }
                        }
                    }
                }
            }
        }
    }
}

async function getBalancePosition(){
    const start = +new Date()

    const options = {
        method: 'POST',
        uri: 'http://localhost:9200/trades/_search?size=999999',
        body: ELASTIC_COOL_QUERY,
        json: true // Automatically stringifies the body to JSON
    }

    const resp = await rp(options)


    console.log(`Time ${(+new Date() - start)/1000} sec.`)

    return resp
}

Promise.resolve().then(async ()=>{
    await getBalancePosition()
})