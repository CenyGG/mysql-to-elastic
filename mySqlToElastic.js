const mysqlStuff = require('mysql');
const {Client} = require('@elastic/elasticsearch')


Promise.resolve().then(async () => {
    await mysqlToElastic()
})

function createMySqlConnection() {
    const connection = mysqlStuff.createConnection({
        host: 'localhost',
        user: 'root',
        password: '111',
        database: 'reports'
    });
    connection.connect();

    return connection
}

function getAllTrades(db) {
    return new Promise((h, r) => {
        db.query('SELECT * from Trades', function (error, results, fields) {
            if (error) r(error);
            h(results)
        });
    })
}

async function mysqlToElastic() {
    const esClient = new Client({node: 'http://localhost:9200'})
    const mysqlDb = createMySqlConnection()

    const trades = await getAllTrades(mysqlDb)
    let counter = 0
    for (let trade of trades) {
        console.log(`${counter++}/${trades.length} (${ Math.floor(counter * 100/trades.length)}%)`)
        await esClient.index({
            index: 'trades',
            id: hashCode(`${trade.apiKeyId}${trade.tradeId}${trade.side}`),
            // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
            body: trade
        })
    }
}

function hashCode(str) {
    return str.split('').reduce((prevHash, currVal) =>
        (((prevHash << 5) - prevHash) + currVal.charCodeAt(0)) | 0, 0);
}